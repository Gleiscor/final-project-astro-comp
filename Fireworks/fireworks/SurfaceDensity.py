import matplotlib.pyplot as plt
import numpy as np
from numpy import inf
import time
from fireworks.particles import Particles
from nbodylib.dynamics import acceleration_pyfalcon
import GenParticles as GP

def SDensCurve(particles: Particles):
    
    N = len(particles.mass)    # number of particles

    M =  particles.mass
    M = M.reshape((len(M),1))
    pos = particles.pos
    vel = particles.vel

    R = np.sqrt((pos[:,0]-pos[0,0])**2 + (pos[:,1]-pos[0,1])**2 + (pos[:,2]-pos[0,2])**2) 
    H,edge=np.histogram(R, bins=20)
    Area = np.pi*(edge[1:]**2 - edge[0:-1]**2)
    Rave = 0.5*(edge[1:] + edge[0:-1]) 
    Sigma = H/Area

    return Sigma, Rave, Area, H, R, edge

