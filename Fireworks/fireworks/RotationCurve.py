import matplotlib.pyplot as plt
import numpy as np
from numpy import inf
import time
from fireworks.particles import Particles
import Shift as sf
import GenParticles as GP
from typing import Optional, Tuple, Callable, Union, List

def RotationalCurveFunction(particles: Particles, theta: Optional[float] = 0., fi: Optional[float] = 0.):
    
    N = len(particles.mass)    # number of particles

    particles = sf.RotatePos(particles, -theta, 0.)
    particles = sf.RotatePos(particles, 0., -fi)

    pos = particles.pos
    vel = particles.vel

    phi = np.arctan2(pos[:,1] - pos[0,1], pos[:,0] - pos[0,0])

    R = np.sqrt((pos[:,0]-pos[0,0])**2 + (pos[:,1]-pos[0,1])**2 + (pos[:,2]-pos[0,2])**2)
    
    #Vphi =  vel[:,1] * np.cos(phi) - vel[:,0] * np.sin(phi)
    Vphi =  (vel[:,1]-vel[0,1]) * np.cos(phi) - (vel[:,0]-vel[0,0]) * np.sin(phi)
    #Vrad = vel[:,0] * np.cos(phi) + vel[:,1] * np.sin(phi)
    Vrad = (vel[:,0]-vel[0,0]) * np.cos(phi) + (vel[:,1]-vel[0,1]) * np.sin(phi)
    return R, Vphi, Vrad

