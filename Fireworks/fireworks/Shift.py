#This program takes in input a class Particles istance and shifts its position and velocities.
#It also rotates the distribution of positions and velocities of an angle theta around the z axis 
#and phi around the y axis (in this order)

import numpy as np
import random
import math 
from fireworks import ic
from fireworks.particles import Particles
#from fireworks.GenParticles import GenerationGalaxyParticles 
import matplotlib.pyplot as plt

#This function shiftes the Particles positions of a given distance in the x, y and z directions
def ShiftPos(particles: Particles, x: float, y: float, z: float):
    vec = [x,y,z]
    particles.pos = particles.pos + vec

    return particles.pos

#This function shiftes the Particles velocities of a given velocity in the x, y and z directions
def ShiftVel(particles: Particles, x: float, y: float, z: float):
    vec = [x,y,z]
    particles.vel = particles.vel + vec

    return particles.vel

#This function rotates the Particles positions and velocities of a given angle theta and phi
def RotatePos(particles: Particles, theta: float, phi: float):
    
    posx = particles.pos[0,0]
    posy = particles.pos[0,1]
    posz = particles.pos[0,2]
    velx = particles.vel[0,0]
    vely = particles.vel[0,1]
    velz = particles.vel[0,2]

    particles.vel = ShiftVel(particles, -velx, -vely, -velz)
    particles.pos = ShiftPos(particles, -posx, -posy, -posz)
    
    rotation_matrix_z = np.array([
        [np.cos(theta), -np.sin(theta), 0],
        [np.sin(theta), np.cos(theta), 0],
        [0, 0, 1]
    ])

    rotation_matrix_y = np.array([
        [np.cos(phi), 0, np.sin(phi)],
        [0, 1, 0],
        [-np.sin(phi), 0, np.cos(phi)]
    ])
    
    rotation_matrix = rotation_matrix_y@rotation_matrix_z
    particles.pos = particles.pos@rotation_matrix
    particles.vel = particles.vel@rotation_matrix

    particles.vel = ShiftVel(particles, velx, vely, velz)
    particles.pos = ShiftPos(particles, posx, posy, posz)

    return particles

def ShiRot(particles: Particles, x: float, y: float, z: float, vx: float, vy: float, vz: float, theta: float, phi: float):
    
    particles = RotatePos(particles, theta, phi)
    particles.pos = ShiftPos(particles, x, y, z)
    particles. vel = ShiftVel(particles, vx, vy, vz)

    return particles
