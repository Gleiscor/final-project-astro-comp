import numpy as np
import bisect
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation
from fireworks.particles import Particles
from nbodylib.dynamics import acceleration_pyfalcon
from nbodylib.integrators import integrator_Galaxy
from mpl_toolkits.mplot3d import Axes3D
from PIL import Image
from IPython.display import display, Image
import SurfaceDensity as SD
import RotationCurve as RC
from typing import Optional, Tuple, Callable, Union, List

def Gif3D(particles : Particles,
           softening : float,
           tfin : float,
           tstep : float,
           Nframes : int,
           fpsGif : int,
           name : str,
           axislim : Optional[List] = None):
    
    """
    particles : the galaxy merge particles obtained with GenParticles.py
    softening : Plummer kernel softening, default set to 5
    tfin : final time of the simulation (like 210)
    tstep : time-step, 0.01, 0.005, 0.001
    Nframes : number of frames the gif has to show, personal advice - between 1% and 5% of the totale 
              time intervals obtained with (tfin - tin)/tstep
    fpsGif : number of frame per second of the Gif - high value = fast Gif, few seconds, low value = too stutter of the Gif
          I suggest setting it to 10
    name : string, the name of Gif (when we save it)
    axislim : Optional list where we set the limit for the x axis (axislim[0]), y and z axes. Axislim must be a list
              in the form (example) [[-50,80], [-20,40], [-20,30]]. Better to put integer values
    """
    #Lenght of galaxy merge

    N = len(particles.mass)

    #Lenght of the time vector t (and maximum number of frames) is

    N_t = int(np.ceil((tfin)/tstep))

    """
    We build the N x 3 x Nframes matrix of all zeros associated to the animation. N is the totale number of particles
    in galaxy merge. Three columns are related to the position X - Y - Z of each particle. 
    Every "vertical section of this three-dimensional matrix is a N x 3 matrix associated to the positions
    of all the N particles in a particular moment in time. For example M[:,:,0] gives the initial positions
    of the particles at time t = 0
    """

    index = np.linspace(0, N_t, Nframes, dtype = int)

    MatrixAnimation = np.zeros((N, 3, Nframes))

    MatrixAnimation[:,:,0] = particles.pos #We set the initial frame of the two galaxies

    #Cycle For to update the positions of all the particles step by step

    for i in range(1, N_t):
        particles, _, _, _, _ = integrator_Galaxy(particles, tstep, softening, None)
        j = bisect.bisect_left(index, i)
        MatrixAnimation[:,:,j] = particles.pos
    
    def update(frame, scat):
        scat._offsets3d = (MatrixAnimation[0:int(N / 2.), 0, frame], MatrixAnimation[0:int(N / 2.), 1, frame], MatrixAnimation[0:int(N / 2.), 2, frame])
        scat2._offsets3d = (MatrixAnimation[int(N / 2.):, 0, frame], MatrixAnimation[int(N / 2.):, 1, frame], MatrixAnimation[int(N / 2.):, 2, frame])
        return scat, scat2
    
    fig = plt.figure(dpi = 150)
    ax = fig.add_subplot(111, projection='3d')

    scat = ax.scatter(MatrixAnimation[0:int(N / 2.), 0, 0], MatrixAnimation[0:int(N / 2.), 1, 0], MatrixAnimation[0:int(N / 2.), 2, 0], color='blue', marker='*', s=5, alpha = 0.5, label = 'Galaxy 1')
    scat2 = ax.scatter(MatrixAnimation[int(N / 2.):, 0, 0], MatrixAnimation[int(N / 2.):, 1, 0], MatrixAnimation[int(N / 2.):, 2, 0], color='red', marker='o', s=5, alpha = 0.5, label = 'Galaxy 2')
    #scat3 = ax.scatter(MatrixAnimation[0,0,0], MatrixAnimation[0,1,0], MatrixAnimation[0,2,0], s = 50, color = 'yellow')

    ax.set_xlabel("x [Nbody]")
    ax.set_ylabel("y [Nbody]")
    ax.set_zlabel("z [Nbody]")
    ax.set_xlim([-40., 80])
    ax.set_ylim([-40., 80])
    ax.set_zlim([-40., 80])

    if axislim is not None:
        xlim = axislim[0]
        ylim = axislim[1]
        zlim = axislim[2]

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_zlim(zlim)
    
    ax.legend(loc = 'best', ncol = 2)

    plt.tight_layout()

    Animation = FuncAnimation(fig, update, frames=np.arange(1, Nframes), interval=200, fargs=(scat,))
    Animation.save(name, writer='pillow', fps=fpsGif)

    return Animation

def Gif2D(particles : Particles,
           softening : float,
           tfin : float,
           tstep : float,
           Nframes : int,
           fpsGif : int,
           name : str,
           axislim : Optional[List] = None,
           projection : Optional[List] = None):
    
    """
    particles : the galaxy merge particles obtained with GenParticles.py
    softening : Plummer kernel softening, default set to 5
    tfin : final time of the simulation (like 210)
    tstep : time-step, 0.01, 0.005, 0.001
    Nframes : number of frames the gif has to show, personal advice - between 1% and 5% of the totale 
              time intervals obtained with (tfin - tin)/tstep
    fpsGif : number of frame per second of the Gif - high value = fast Gif, few seconds, low value = too stutter of the Gif
          I suggest setting it to 10
    name : string, the name of Gif (when we save it)
    axislim : Optional list where we set the limit for the x axis (axislim[0]) and y axis. Axislim must be a list
              in the form (example) [[-50,80], [-20,40]]. Better to put integer values
    projection : Optional list where we choose the plane for the projection (integer values)
                - XY : [0,1]
                - XZ : [0,2]
                - YZ : [1,2]
    """
    #Lenght of galaxy merge

    N = len(particles.mass)
    N_t = int(np.ceil((tfin)/tstep))

    planeXY = False
    planeXZ = False
    planeYZ = False

    index = np.linspace(0, N_t, Nframes, dtype=int)
    MatrixAnimation = np.zeros((N, 2, Nframes))  # 2D matrix for the projection

    if (projection[0] == 0 and projection[1] == 1):
        planeXY = True
    elif (projection[0] == 0 and projection[1] == 2):
        planeXZ = True
    elif (projection[0] == 1 and projection[1] == 2):
        planeYZ = True
    
    if planeXY:
        MatrixAnimation[:,:,0] = particles.pos[:,projection]  # Consider only x and y coordinates
    elif planeXZ:
        MatrixAnimation[:,:,0] = particles.pos[:,projection]  # Consider only x and z coordinates
    elif planeYZ:
        MatrixAnimation[:,:,0] = particles.pos[:,projection]  # Consider only y and z coordinates

    for i in range(1, N_t):
        particles, _, _, _, _ = integrator_Galaxy(particles, tstep, 5, None)
        j = bisect.bisect_left(index, i)
        MatrixAnimation[:,:,j] = particles.pos[:,projection]  # Update only x and y coordinates
    
    def update(frame, scat):
        scat.set_offsets(MatrixAnimation[:int(N / 2), :, frame])  # First half of the galaxy merge with color : blue
        scat2.set_offsets(MatrixAnimation[int(N / 2):, :, frame])  # Second half of the galaxy merge with color : red
        scat3.set_offsets(MatrixAnimation[0,:,frame])
        scat4.set_offsets(MatrixAnimation[int(N/2.),:,frame])
        return scat, scat2, scat3, scat4

    fig, ax = plt.subplots(dpi=150)

    if planeXY:
        ax.set_xlabel("x [Nbody]")
        ax.set_ylabel("y [Nbody]")
    elif planeXZ:
        ax.set_xlabel("x [Nbody]")
        ax.set_ylabel("z [Nbody]")
    elif planeYZ:
        ax.set_xlabel("y [Nbody]")
        ax.set_ylabel("z [Nbody]")
        
    ax.set_xlim([-40., 80])
    ax.set_ylim([-40., 80])

    if axislim is not None:
        xlim = axislim[0]
        ylim = axislim[1]

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

    scat = ax.scatter(MatrixAnimation[:int(N / 2), 0, 0], MatrixAnimation[:int(N / 2), 1, 0], color='blue', marker='*', s=5, label='Galaxy 1')
    scat2 = ax.scatter(MatrixAnimation[int(N / 2):, 0, 0], MatrixAnimation[int(N / 2):, 1, 0], color='red', marker='o', s=5, label='Galaxy 2')
    scat3 = ax.scatter(MatrixAnimation[0,0,0], MatrixAnimation[0,1,0], color = 'yellow', s = 50, label = 'Mass 1')
    scat4 = ax.scatter(MatrixAnimation[int(N/2.),0,0], MatrixAnimation[int(N/2.),1,0], color = 'lime', s = 50, label = 'Mass 2')

    ax.legend(loc='lower right', ncol=2, fontsize = 'small')
    plt.tight_layout()

    Animation = FuncAnimation(fig, update, frames=np.arange(1, Nframes), interval=200, fargs=(scat,))
    Animation.save(name, writer='pillow', fps=fpsGif)

    return Animation

def GifVelCurve(particles : Particles,
                softening : float,
                tfin : float,
                tstep : float,
                Nframes : int,
                fpsGif : int,
                nameVphi : str,
                nameVrad : str):

    N = len(particles.mass)
    N_t = int(np.ceil((tfin)/tstep))

    index = np.linspace(0, N_t, Nframes, dtype=int)
    M_AniVphi = np.zeros((N, 2, Nframes))  # 2D matrix for R and Vphi coordinates
    M_AniVrad = np.zeros((N, 2, Nframes))  # 2D matrix for R and Vrad coordinates

    Ro, Vphio, Vrado = RC.RotationalCurveFunction(particles)

    M_AniVphi[:,:,0] = np.column_stack((Ro, Vphio))
    M_AniVrad[:,:,0] = np.column_stack((Ro, Vrado))

    for i in range(1, N_t):
        particles, _, _, _, _ = integrator_Galaxy(particles, tstep, 5, None)
        R, Vphi, Vrad = RC.RotationalCurveFunction(particles)
        j = bisect.bisect_left(index, i)
        M_AniVphi[:,:,j] = np.column_stack((R, Vphi))
        M_AniVrad[:,:,j] = np.column_stack((R, Vrad))
    
    def Update_Vphi(frame, scatVphi):
        scatVphi.set_offsets(M_AniVphi[:, :, frame])
        return scatVphi
    
    def Update_Vrad(frame, scatVrad):
        scatVrad.set_offsets(M_AniVrad[:, :, frame])
        return scatVrad
    
    figVphi, axVphi = plt.subplots(dpi=150)
    axVphi.set_xlabel("R [Nbody]")
    axVphi.set_ylabel("$V_{\\phi}$ [Nbody]")

    scatVphi = axVphi.scatter(M_AniVphi[:, 0, 0], M_AniVphi[:, 1, 0], color = 'blue', marker = 'o', s = 1)
    plt.tight_layout()

    """
    figVrad, axVrad = plt.subplots(dpi=150)
    axVrad.set_xlabel("R [Nbody]")
    axVrad.set_ylabel("$V_{\\rm rad}$ [Nbody]")

    scatVrad = axVrad.scatter(M_AniVrad[:, 0, 0], M_AniVrad[:, 1, 0], color = 'blue', marker = 'o', s = 1)
    plt.tight_layout()
    """

    AnimationVphi = FuncAnimation(figVphi, Update_Vphi, frames=np.arange(1, Nframes), interval=200, fargs=(scatVphi,))
    AnimationVphi.save(nameVphi, writer='pillow', fps=fpsGif)

    #AnimationVrad = FuncAnimation(figVrad, Update_Vrad, frames=np.arange(1, Nframes), interval=200, fargs=(scatVrad,))
    #AnimationVrad.save(nameVrad, writer='pillow', fps=fpsGif)
    plt.show()
    
    return AnimationVphi


    """
    fig, (axVphi, axVrad) = plt.subplots(1, 2, figsize=(10, 5), dpi=150)
    
    axVphi.set_xlabel("R [Nbody]")
    axVphi.set_ylabel("$V_{\\phi}$ [Nbody]")
    axVrad.set_xlabel("R [Nbody]")
    axVrad.set_ylabel("$V_{\\rm rad}$ [Nbody]")

    scatVphi = axVphi.scatter(M_AniVphi[:, 0, 0], M_AniVphi[:, 1, 0], color='blue', marker='o', s=1)
    scatVrad = axVrad.scatter(M_AniVrad[:, 0, 0], M_AniVrad[:, 1, 0], color='blue', marker='o', s=1)
    plt.tight_layout()

    def init():
        return scatVphi, scatVrad

    def update(frame):
        scatVphi.set_offsets(M_AniVphi[:, :, frame])
        scatVrad.set_offsets(M_AniVrad[:, :, frame])
        return scatVphi, scatVrad

    Animation = FuncAnimation(fig, update, frames=np.arange(1, Nframes), init_func=init, interval=200, blit=True)

    Animation.save(nameVphi, writer='pillow', fps=fpsGif)
    Animation.save(nameVrad, writer='pillow', fps=fpsGif)

    return Animation
    """

def Gif3D1(particles : Particles,
           softening : float,
           tfin : float,
           tstep : float,
           Nframes : int,
           fpsGif : int,
           name : str,
           axislim : Optional[List] = None):
    
    """
    particles : the galaxy merge particles obtained with GenParticles.py
    softening : Plummer kernel softening, default set to 5
    tfin : final time of the simulation (like 210)
    tstep : time-step, 0.01, 0.005, 0.001
    Nframes : number of frames the gif has to show, personal advice - between 1% and 5% of the totale 
              time intervals obtained with (tfin - tin)/tstep
    fpsGif : number of frame per second of the Gif - high value = fast Gif, few seconds, low value = too stutter of the Gif
          I suggest setting it to 10
    name : string, the name of Gif (when we save it)
    axislim : Optional list where we set the limit for the x axis (axislim[0]), y and z axes. Axislim must be a list
              in the form (example) [[-50,80], [-20,40], [-20,30]]. Better to put integer values
    """
    #Lenght of galaxy merge

    N = len(particles.mass)

    #Lenght of the time vector t (and maximum number of frames) is

    N_t = int(np.ceil((tfin)/tstep))

    """
    We build the N x 3 x Nframes matrix of all zeros associated to the animation. N is the totale number of particles
    in galaxy merge. Three columns are related to the position X - Y - Z of each particle. 
    Every "vertical section of this three-dimensional matrix is a N x 3 matrix associated to the positions
    of all the N particles in a particular moment in time. For example M[:,:,0] gives the initial positions
    of the particles at time t = 0
    """

    index = np.linspace(0, N_t, Nframes, dtype = int)

    MatrixAnimation = np.zeros((N, 3, Nframes))

    MatrixAnimation[:,:,0] = particles.pos #We set the initial frame of the two galaxies

    #Cycle For to update the positions of all the particles step by step

    for i in range(1, N_t):
        particles, _, _, _, _ = integrator_Galaxy(particles, tstep, softening, None)
        j = bisect.bisect_left(index, i)
        MatrixAnimation[:,:,j] = particles.pos
    
    def update(frame, scat):
        scat._offsets3d = (MatrixAnimation[0:int(N), 0, frame], MatrixAnimation[0:int(N), 1, frame], MatrixAnimation[0:int(N), 2, frame])
        return scat
    
    fig = plt.figure(dpi = 150)
    ax = fig.add_subplot(111, projection='3d')

    scat = ax.scatter(MatrixAnimation[0:int(N), 0, 0], MatrixAnimation[0:int(N), 1, 0], MatrixAnimation[0:int(N), 2, 0], color='blue', marker='*', s=5, alpha = 0.5, label = 'Galaxy 1')
    #scat3 = ax.scatter(MatrixAnimation[0,0,0], MatrixAnimation[0,1,0], MatrixAnimation[0,2,0], s = 50, color = 'yellow')

    ax.set_xlabel("x [Nbody]")
    ax.set_ylabel("y [Nbody]")
    ax.set_zlabel("z [Nbody]")
    ax.set_xlim([-40., 80])
    ax.set_ylim([-40., 80])
    ax.set_zlim([-40., 80])

    if axislim is not None:
        xlim = axislim[0]
        ylim = axislim[1]
        zlim = axislim[2]

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_zlim(zlim)
    
    ax.legend(loc = 'best', ncol = 2)

    plt.tight_layout()

    Animation = FuncAnimation(fig, update, frames=np.arange(1, Nframes), interval=200, fargs=(scat,))
    Animation.save(name, writer='pillow', fps=fpsGif)

    return Animation