#This program takes in input a CSV file that contains the values simulated from a Plummer kernel and generates a fireworks Particles instance

import numpy as np
import matplotlib.pyplot as plt
import random
from fireworks import ic
from fireworks.particles import Particles
import Shift as sf
from typing import Optional, Tuple, Callable, Union, List

#We build a function "GenerationGalaxyParticles" that reads the full CSV file and return the Particles instances associated to the mass, positions and velocities

def GenerationGalaxyParticles(file_csv_name, N, pos_shift, vel_shift, theta, phi):

    #Pos_shift and vel_shift are related to the traslation of the second galaxy, theta and phi its rotations along z and y axis respectively

    #We indicate with the subscript 0 the initial elements generated from the Plummer distribution

    m0, x0, y0, z0, vx0, vy0, vz0 = np.genfromtxt(file_csv_name, comments = '#', delimiter = ',', skip_header = 1, usecols = (0,1,2,3,4,5,6), unpack = True)

    L = len(m0) #Lenght of the complete data set

    #We can/must choose one of the different possibilities 

    #First possibility : take the initial N rows (since they are not sorted, so there is no bias in taking just the first N rows)

    def FirstNPickUp(N):
        
        m, x, y, z, vx, vy, vz = m0[0:N], x0[0:N], y0[0:N], z0[0:N], vx0[0:N], vy0[0:N], vz0[0:N]
        return m, x, y, z, vx, vy, vz

    #Second possibility : select random N-1 integer indices (first line is fixed) 

    def RandomPickUp(L, N):

        #Let's define the list for the new N-1 / N elements 

        m, x, y, z, vx, vy, vz = [m0[0]], [x0[0]], [y0[0]], [z0[0]], [vx0[0]], [vy0[0]], [vz0[0]]

        def RandomSampleIndices(L, N): 
            
            #L is the initial lenght of m0, x0, ecc; N the number of elements we want to deal with

            random_indices = random.sample(range(1,L), N-1)
            return random_indices
        
        indices = RandomSampleIndices(L, N)

        mi = [m0[i] for i in indices]
        m = m + mi
        xi = [x0[i] for i in indices]
        x = x + xi
        yi = [y0[i] for i in indices]
        y = y + yi
        zi = [z0[i] for i in indices]
        z = z + zi
        vxi = [vx0[i] for i in indices]
        vx = vx + vxi
        vyi = [vy0[i] for i in indices]
        vy = vy + vyi
        vzi = [vz0[i] for i in indices]
        vz = vz + vzi

        return np.array(m), np.array(x), np.array(y), np.array(z), np.array(vx), np.array(vy), np.array(vz)
    
    m, x, y, z, vx, vy, vz = RandomPickUp(L, N) #or

    #m, x, y, z, vx, vy, vz = FirstNPickUp(L,N) 

    def GenParticles(m,x,y,z,vx,vy,vz):

        pos = np.column_stack([x, y, z])
        vel = np.column_stack([vx, vy, vz])
        particles = Particles(pos, vel, m)
        return particles
    
    #We get the particles associated to the first galaxy : galaxy_one

    galaxy_one = GenParticles(m,x,y,z,vx,vy,vz)

    """

    For the second galaxy : galaxy_two we give two options. 
    On one hand we build the second galaxy as a copy of the first one. Then, using pos_shift and vel_shift, which are
    the vectors associated to the translation of each particle (position and velocity), we translate the second galaxy.
    Then, the angle theta and phi are related respectively to the rotation of the galaxy along the z and y axis. 
    On the other hand we draw randomly another set of N particles from the file "Nbody_disc.csv" for the galaxy_two. Then
    we translate and rotate the galaxy analogously as we could do in the first method. 

    """

    def GalaxyTwoCopyShift(galaxy_one, pos_shift, vel_shift, theta, phi):

        galaxy_two = galaxy_one.copy() #We copy the first particles galaxy_one and then we apply translation and rotation

        xshift, yshift, zshift = pos_shift[0], pos_shift[1], pos_shift[2]
        vxshift, vyshift, vzshift = vel_shift[0], vel_shift[1], vel_shift[2]

        galaxy_two = sf.ShiRot(galaxy_two, xshift, yshift, zshift, vxshift, vyshift, vzshift, theta, phi)

        return galaxy_two

    def GalaxyTwoRandomPickUp(pos_shift, vel_shift, theta, phi):

        m2, x2, y2, z2, vx2, vy2, vz2 = RandomPickUp(L, N)
        galaxy_two = GenParticles(m2,x2,y2,z2,vx2,vy2,vz2)

        xshift, yshift, zshift = pos_shift[0], pos_shift[1], pos_shift[2]
        vxshift, vyshift, vzshift = vel_shift[0], vel_shift[1], vel_shift[2]

        galaxy_two = sf.ShiRot(galaxy_two, xshift, yshift, zshift, vxshift, vyshift, vzshift, theta, phi)

        return galaxy_two

    #We get galaxy_two (we can choose which method we prefer)

    #galaxy_two = GalaxyTwoCopyShift(galaxy_one, pos_shift, vel_shift, theta, phi)

    galaxy_two = GalaxyTwoRandomPickUp(pos_shift, vel_shift, theta, phi)

    def GenGalaxyMerge(galaxy_one, galaxy_two):

        pos_merge = np.concatenate((galaxy_one.pos, galaxy_two.pos))
        vel_merge = np.concatenate((galaxy_one.vel, galaxy_two.vel))
        mass_merge = np.concatenate((galaxy_one.mass, galaxy_two.mass))

        #we build a larger list containing both galaxy_one and galaxy_two particles. Then we use the class Particles to get pos, vel and mass of the galaxy_merge
        
        galaxy_merge = Particles(pos_merge, vel_merge, mass_merge)

        return galaxy_merge

    galaxy_merge = GenGalaxyMerge(galaxy_one, galaxy_two)

    return galaxy_merge

def GaussianNoise(particles: Particles, theta, fi, sR, sphi, sz):
    
    N = int(len(particles.mass)/2)

    galaxy_one = Particles(particles.pos[:N,:], particles.vel[:N,:], particles.mass[:N])
    galaxy_two = Particles(particles.pos[N:,:], particles.vel[N:,:], particles.mass[N:])

    x = galaxy_two.pos[0,0]
    y = galaxy_two.pos[0,1]
    z = galaxy_two.pos[0,2]
    vx = galaxy_two.vel[0,0]
    vy = galaxy_two.vel[0,0]
    vz = galaxy_two.vel[0,0]
    zero = np.zeros(N-1)

    galaxy_two = sf.RotatePos(galaxy_two, -theta, 0.)
    galaxy_two = sf.ShiRot(galaxy_two, -x, -y, -z, -vx, -vy, -vz, 0., -fi)

    phi1 = np.arctan2(galaxy_one.pos[1:,1] - galaxy_one.pos[0,1], galaxy_one.pos[1:,0] - galaxy_one.pos[0,0])
    phi2 = np.arctan2(galaxy_two.pos[1:,1] - galaxy_two.pos[0,1], galaxy_two.pos[1:,0] - galaxy_two.pos[0,0])
    
    galaxy_one.vel[1:,2] += np.random.normal(zero, sz) 
    galaxy_one.vel[1:,0] += np.cos(phi1)*np.random.normal(zero, sR) - np.sin(phi1)*np.random.normal(zero, sphi) 
    galaxy_one.vel[1:,1] += np.sin(phi1)*np.random.normal(zero, sR) + np.cos(phi1)*np.random.normal(zero, sphi)

    galaxy_two.vel[1:,2] += np.random.normal(zero, sz) 
    galaxy_two.vel[1:,0] += np.cos(phi2)*np.random.normal(zero, sR) - np.sin(phi2)*np.random.normal(zero, sphi) 
    galaxy_two.vel[1:,1] += np.sin(phi2)*np.random.normal(zero, sR) + np.cos(phi2)*np.random.normal(zero, sphi)

    galaxy_two = sf.ShiRot(galaxy_two, x, y, z, vx, vy, vz, theta, fi)

    particles.pos[:N,:] = galaxy_one.pos[:,:]
    particles.vel[:N,:] = galaxy_one.vel[:,:]

    particles.pos[N:,:] = galaxy_two.pos[:,:]
    particles.vel[N:,:] = galaxy_two.vel[:,:]

    return particles
