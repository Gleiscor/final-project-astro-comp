import matplotlib.pyplot as plt
import numpy as np
from numpy import inf
import time
from fireworks.particles import Particles
from nbodylib.dynamics import acceleration_pyfalcon
import GenParticles as GP
   
def main():

    pos_shift = np.array([40., 20., 0.])
    vel_shift = np.array([-0.1, 0., 0.])
    theta = np.pi
    phi = np.pi/6.
    
    galaxy_merge = GP.GenerationGalaxyParticles("Nbody_disc.csv", 1000, pos_shift, vel_shift, theta, phi)



    N         = len(galaxy_merge.mass)    # number of particles
    t         = 0      # current time of the simulation
    endTime   = 500   # time at which simulation ends
    dt        = 0.01   # timestep
    limit     = 0.1    # limit length
    G         = 1.0    # value of Gravitational Constant used for the simulation
    
    M   =  galaxy_merge.mass
    M  = M.reshape((len(M),1))
    R   = galaxy_merge.pos
    vel = galaxy_merge.vel
    
    # Convert to Center-of-M frame
    vel -= np.mean(M * vel,0) / np.mean(M)
    
    # calculate initial gravitational accelerations

    acc, _, _ = acceleration_pyfalcon(galaxy_merge, 5.)
    
    # number of timesteps
    Nt = int(np.ceil(endTime/dt))
    
    # save particle orbits for plotting trails
    position_save = np.zeros((N, 3, Nt+1))
    position_save[:,:,0] = R
    
    # figure
    #plt.style.use('dark_background')

    """
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(R[0:int(N/2.),0], R[0:int(N/2.),1], R[0:int(N/2.),2], color = 'blue', marker = '*', s = 5)
    ax.scatter(R[int(N/2.)+1:,0], R[int(N/2.)+1:,1], R[int(N/2.)+1:,2], color = 'red', marker = 'o', s = 5)
    plt.savefig("PreMerge.png")
    """
    fig = plt.figure()
    ax = fig.add_subplot(projection = '3d')

    # simulation loop
    for i in range(Nt):
        vel += acc * dt/2.0
        R += vel * dt
        acc, _, _ = acceleration_pyfalcon(galaxy_merge, 5.)
        vel += acc * dt/2.0
        t += dt
        #position_save[:,:,i+1] = R

        """
        if (i == int(Nt/2.)):
            fig3 = plt.figure()
            ax3 = fig3.add_subplot(projection='3d')
            ax3.set_title("Middle steps")
            ax3.scatter(R[0:int(N/2.),0], R[0:int(N/2.),1], R[0:int(N/2.),2], color = 'blue', marker = '*', s = 5)
            ax3.scatter(R[int(N/2.)+1:,0], R[int(N/2.)+1:,1], R[int(N/2.)+1:,2], color = 'red', marker = 'o', s = 5)
        """

        if (i % 100 == 0):
            plt.cla()
            ax.scatter(R[0:int(N/2.),0], R[0:int(N/2.),1], R[0:int(N/2.),2], color = 'blue', marker = '*', s = 5)
            ax.scatter(R[int(N/2.)+1:,0], R[int(N/2.)+1:,1], R[int(N/2.)+1:,2], color = 'red', marker = 'o', s = 5)
            #xp = position_save[:, 0, max(i-50,0):i+1]
            #yp = position_save[:, 1, max(i-50,0):i+1]
            #plt.scatter(xp, yp, s=1, color = 'cornflowerblue')
            #plt.scatter(R[:,0], R[:,1], s=10, color ='white')
            plt.pause(0.001)
    
    plt.show()

    #return 0
    
    """
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(projection = '3d')
    ax2.set_title("Merge Finale")
    ax2.scatter(R[0:int(N/2.),0], R[0:int(N/2.),1], R[0:int(N/2.),2], color = 'blue', marker = '*', s = 5)
    ax2.scatter(R[int(N/2.)+1:,0], R[int(N/2.)+1:,1], R[int(N/2.)+1:,2], color = 'red', marker = 'o', s = 5)
    plt.savefig("AfterMerge.png")
    plt.show()
    """

if __name__== "__main__":
  main()