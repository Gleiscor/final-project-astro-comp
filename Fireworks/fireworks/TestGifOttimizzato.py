import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation
from fireworks.particles import Particles
from nbodylib.dynamics import acceleration_pyfalcon
import GenParticles as GP
from mpl_toolkits.mplot3d import Axes3D
from PIL import Image
from IPython.display import display, Image
import GifFunction as GIF

pos_shift = np.array([40., 20., 30.])
vel_shift = np.array([-0.2, 0., 0.])
theta = np.pi/3.
phi = np.pi/4.

N = 1000

galaxy_merge = GP.GenerationGalaxyParticles("Nbody_disc.csv", N, pos_shift, vel_shift, theta, phi)

"""

N = len(galaxy_merge.mass)    # number of particles
t = 0      # current time of the simulation
endTime = 210   # time at which simulation ends
dt = 0.01   # timestep
limit = 0.1    # limit length
G = 1.0

M = galaxy_merge.mass
R = galaxy_merge.pos
vel = galaxy_merge.vel

acc, _, _ = acceleration_pyfalcon(galaxy_merge, 5.)

Nt = int(np.ceil(endTime / dt))

matrix = np.zeros((N, 3, Nt + 1))
matrix[:, :, 0] = R

for i in range(1, Nt):
    vel += acc * dt / 2.0
    R += vel * dt
    acc, _, _ = acceleration_pyfalcon(galaxy_merge, 5.)
    vel += acc * dt / 2.0
    t += dt
    matrix[:, :, i] = R

def update(frame, scat):
    scat._offsets3d = (matrix[0:int(N / 2.), 0, frame], matrix[0:int(N / 2.), 1, frame], matrix[0:int(N / 2.), 2, frame])
    scat2._offsets3d = (matrix[int(N / 2.) + 1:, 0, frame], matrix[int(N / 2.) + 1:, 1, frame], matrix[int(N / 2.) + 1:, 2, frame])
    return scat, scat2

figProvaAnim = plt.figure(dpi=100)
ax = figProvaAnim.add_subplot(111, projection='3d')

scat = ax.scatter(matrix[0:int(N / 2.), 0, 0], matrix[0:int(N / 2.), 1, 0], matrix[0:int(N / 2.), 2, 0], color='blue', marker='*', s=5)
scat2 = ax.scatter(matrix[int(N / 2.) + 1:, 0, 0], matrix[int(N / 2.) + 1:, 1, 0], matrix[int(N / 2.) + 1:, 2, 0], color='red', marker='o', s=5)

ani = FuncAnimation(figProvaAnim, update, frames=np.arange(1, Nt, 150), interval=150, fargs=(scat,))

#ani.save('Merge.gif', writer='pillow', fps=10)
plt.show()

"""

#Animation = GIF.Gif3D(galaxy_merge, 5, 500, 0.01, 300, 20, "ProvaTrasparency.gif", [[-60,80], [-40, 80], [-50, 30]])

Animation1 = GIF.Gif2D(galaxy_merge, 5, 300, 0.01, 400, 20,"ProvaGif2D.gif", [[-50,50], [-40, 60]], [1,2])

#galaxy_one = Particles(galaxy_merge.pos[:N,:], galaxy_merge.vel[:N,:], galaxy_merge.mass[:N])

#AnimationVphi = GIF.GifVelCurve(galaxy_one, 5, 210, 0.01, 500, 10, "VphiProva.gif", "VradProva.gif")



