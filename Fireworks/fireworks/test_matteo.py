import GenParticles as GP
import numpy as np
from fireworks.particles import Particles
import copy as cp
import matplotlib.pyplot as plt

galaxy_merge = GP.GenerationGalaxyParticles("Nbody_disc.csv", 10000)

figProva = plt.figure(dpi = 300)
ax = figProva.add_subplot(projection = '3d')

ax.scatter(galaxy_merge.pos[:,0], galaxy_merge.pos[:,1], galaxy_merge.pos[:,2], marker = 'o', s = 2)
#ax.scatter(galaxy_two.pos[:,0], galaxy_two.pos[:,1], galaxy_two.pos[:,2], marker = 'o', s = 2)
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")
#plt.show()
M = galaxy_merge.mass
print(M.shape)
b  = M.reshape((len(M),1))    # b.shape  = (3,1)
print(b.shape)