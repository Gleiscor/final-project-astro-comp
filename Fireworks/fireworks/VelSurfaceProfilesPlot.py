import numpy as np
import matplotlib.pyplot as plt
from fireworks.particles import Particles
import fireworks.nbodylib.integrators as fint
from nbodylib.dynamics import acceleration_pyfalcon
from nbodylib.integrators import integrator_Galaxy
import SurfaceDensity as SD
import RotationCurve as RC
import numpy.typing as npt
from typing import Optional, Tuple, Callable, Union, List

def StatisticalAnalysis(Vaz1F : npt.NDArray[np.float64],
                        Vaz2F : npt.NDArray[np.float64],
                        Vrad1F : npt.NDArray[np.float64],
                        Vrad2F : npt.NDArray[np.float64],
                        R1F : npt.NDArray[np.float64],
                        R2F : npt.NDArray[np.float64],
                        ext : float):
    
    """
    Vaz1F : final azimuthal velocity array of galaxy 1
    Vaz2F : final azimuthal velocity array of galaxy 2
    Vrad1F : final radial velocity array of galaxy 1
    Vrad2F : final radial velocity array of galaxy 2
    """

    counter1 = 0 #Counter for galaxy 1
    counter2 = 0 #Counter for galaxy 2
    Vphi1 = np.zeros(1, float) #Array to store the azimuthal velocities of particles with R < 30 for galaxy 1
    Vphi2 = np.zeros(1, float) #Array to store the azimuthal velocities of particles with R < 30 for galaxy 2
    Vrad1 = np.zeros(1, float) #Array to store the radial velocities of particles with R < 30 for galaxy 1
    Vrad2 = np.zeros(1, float) #Array to store the radial velocities of particles with R < 30 for galaxy 2

    n = len(R1F)
    for i in range(n):
        if (R1F[i] < 30*ext):
            Vphi1 = np.append(Vphi1, Vaz1F[i])
            Vrad1 = np.append(Vrad1, Vrad1F[i])
            counter1 += 1
        if (R2F[i] < 30.):
            Vphi2 = np.append(Vphi2, Vaz2F[i])
            Vrad2 = np.append(Vrad2, Vrad2F[i])
            counter2 += 1

    MeanAzi1 = np.round(np.mean(Vphi1), decimals = 2)
    MeanAzi2 = np.round(np.mean(Vphi2), decimals = 2)
    MeanRad1 = np.round(np.mean(Vrad1), decimals = 2)
    MeanRad2 = np.round(np.mean(Vrad2), decimals = 2)
    StdAzi1 = np.round(np.std(Vphi1), decimals = 2)
    StdAzi2 = np.round(np.std(Vphi2), decimals = 2)
    StdRad1 = np.round(np.std(Vrad1), decimals = 2)
    StdRad2 = np.round(np.std(Vrad2), decimals = 2)
    Ratio1 = counter1/len(Vaz1F) * 100 #In %
    Ratio2 = counter2/len(Vaz2F) * 100 #In %

    print("The mean azimuthal velocity of the first galaxy is ", MeanAzi1, "with standard deviation ", StdAzi1)
    print("The mean azimuthal velocity of the second galaxy is ", MeanAzi2, "with standard deviation ", StdAzi2)
    print("The mean radial velocity of the first galaxy is ", MeanRad1, "with standard deviation ", StdRad1)
    print("The mean radial velocity of the second galaxy is ", MeanRad2, "with standard deviation ", StdRad2)
    print("The percentage of the particles that remained in the first galaxy (R<", 30*ext, ") is ", Ratio1, "%")
    print("The percentage of the particles that remained in the second galaxy (R<30) is ", Ratio2, "%")


def PlotVelSurfaceCurves(particles : Particles,
                         softening : float,
                         tfin : float,
                         tstep : float,
                         nameplot1 : str,
                         nameplot2 : str,
                         theta : float = 0.,
                         phi : float = 0.,
                         ext : float = 1.):
    
    """
    nameplot1 : name of the plots related to Galaxy 1 - ex: "Fig5_1" WITHOUTH .png, for Chapter 5 Run One Encounter
    nameplot2 : name of the plots related to Galaxy 2 - ex: "Fig5_2" WITHOUTH .png, for Chapter 5 Run One Encounter
    """
    
    N = int(len(particles.mass)/2.)

    galaxy_oneI = Particles(particles.pos[:N,:], particles.vel[:N,:], particles.mass[:N]) #Initial configuration galaxy 1
    galaxy_twoI = Particles(particles.pos[N:,:], particles.vel[N:,:], particles.mass[N:]) #Initial configuration galaxy 2

    #We denote with I the variables related to the initial configuration of the galaxies. F the final variables.

    #The subscripts 1,2 I/F correspond to galaxy 1 and 2 and the beginning (I : t = 0) or the end (F : t = tfin) of the simulation

    Sigma1I, Rave1I, Area1I, _, _, edge1I = SD.SDensCurve(galaxy_oneI) 
    R1I, Vaz1I, Vrad1I = RC.RotationalCurveFunction(galaxy_oneI)

    Sigma2I, Rave2I, Area2I, _, _, edge2I = SD.SDensCurve(galaxy_twoI) 
    R2I, Vaz2I, Vrad2I = RC.RotationalCurveFunction(galaxy_twoI, theta, phi)

    #Now let's evolve the system with our integrator_galaxy function. 

    Nt = int(np.ceil(tfin/tstep)) #The number of time steps depending on the evolution time (ex 210 in N body units and 0.01 for the time-step)

    for i in range(Nt):
        particles, _, _, _, _ = integrator_Galaxy(particles, tstep, softening, None)

    galaxy_oneF = Particles(particles.pos[:N,:], particles.vel[:N,:], particles.mass[:N]) #Galaxy 1 after the evolution
    galaxy_twoF = Particles(particles.pos[N:,:], particles.vel[N:,:], particles.mass[N:]) #Galaxy 2 after the evolution

    Sigma1F, Rave1F, Area1F, _, _, edge1F = SD.SDensCurve(galaxy_oneF) 
    R1F, Vaz1F, Vrad1F = RC.RotationalCurveFunction(galaxy_oneF)
    Sigma2F, Rave2F, Area2F, _, _, edge2F = SD.SDensCurve(galaxy_twoF) 
    R2F, Vaz2F, Vrad2F = RC.RotationalCurveFunction(galaxy_twoF, theta, phi)

    fig1, ax1 = plt.subplots(3,2, figsize = (11,10))

    ax1[0,0].scatter(R1I, Vaz1I, s = 1, label = 'Evolution time : t = 0', color = 'blue')
    ax1[0,0].set_xlabel('$R_{cyl}$ [nbody units]')
    ax1[0,0].set_ylabel('$V_{\\phi}$ [nbody units]')
    ax1[0,0].set_title("Azimuthal velocity profile")
    ax1[0,0].legend(loc = 'best')
    ax1[1,0].scatter(R1I, Vrad1I, s = 1, label = 'Evolution time : t = 0', color = 'blue')
    ax1[1,0].set_xlabel('$R_{cyl}$ [nbody units]')
    ax1[1,0].set_ylabel('$V_{rad}$ [nbody units]')
    ax1[1,0].set_title("Radial velocity profile")
    ax1[1,0].legend(loc = 'best')
    ax1[2,0].plot(Rave1I, Sigma1I, label = 'Evolution time : t = 0', color = 'blue')
    ax1[2,0].set_yscale("log")
    ax1[2,0].set_xlabel('$R_{cyl}$ [nbody units]')
    ax1[2,0].set_ylabel('$\Sigma(R_{cyl})$ [nbody units]')
    ax1[2,0].set_title('Surface density profile')
    ax1[2,0].legend(loc = 'best')
    ax1[0,1].scatter(R1F, Vaz1F, s = 1, label = f'Evolution time : t = {tfin}', color = 'blue')
    ax1[0,1].set_xlabel('$R_{cyl}$ [nbody units]')
    ax1[0,1].set_ylabel('$V_{\\phi}$ [nbody units]')
    ax1[0,1].set_title("Azimuthal velocity profile")
    ax1[0,1].legend(loc = 'best')
    ax1[1,1].scatter(R1F, Vrad1F, s = 1, label = f'Evolution time : t = {tfin}', color = 'blue')
    ax1[1,1].set_xlabel('$R_{cyl}$ [nbody units]')
    ax1[1,1].set_ylabel('$V_{rad}$ [nbody units]')
    ax1[1,1].set_title("Radial velocity profile")
    ax1[1,1].legend(loc = 'best')
    ax1[2,1].plot(Rave1F, Sigma1F, label = f'Evolution time : t = {tfin}', color = 'blue')
    ax1[2,1].set_yscale("log")
    ax1[2,1].set_xlabel('$R_{cyl}$ [nbody units]')
    ax1[2,1].set_ylabel('$\Sigma(R_{cyl})$ [nbody units]')
    ax1[2,1].set_title('Surface density profile')
    ax1[2,1].legend(loc = 'best')
    fig1.suptitle(f'{nameplot1}: Azimuthal velocity profile, radial velocity profile and surface density profile for galaxy one')
    plt.tight_layout()
    fig1.savefig(f"{nameplot1}.png")
    plt.show()

    fig2, ax2 = plt.subplots(3,2, figsize = (11,10))

    ax2[0,0].scatter(R2I, Vaz2I, s = 1, label = 'Evolution time : t = 0', color = 'red')
    ax2[0,0].set_xlabel('$R_{cyl}$ [nbody units]')
    ax2[0,0].set_ylabel('$V_{\\phi}$ [nbody units]')
    ax2[0,0].set_title("Azimuthal velocity profile")
    ax2[0,0].legend(loc = 'best')
    ax2[1,0].scatter(R2I, Vrad2I, s = 1, label = 'Evolution time : t = 0', color = 'red')
    ax2[1,0].set_xlabel('$R_{cyl}$ [nbody units]')
    ax2[1,0].set_ylabel('$V_{rad}$ [nbody units]')
    ax2[1,0].set_title("Radial velocity profile")
    ax2[1,0].legend(loc = 'best')
    ax2[2,0].plot(Rave2I, Sigma2I, label = 'Evolution time : t = 0', color = 'red')
    ax2[2,0].set_yscale("log")
    ax2[2,0].set_xlabel('$R_{cyl}$ [nbody units]')
    ax2[2,0].set_ylabel('$\Sigma(R_{cyl})$ [nbody units]')
    ax2[2,0].set_title('Surface density profile')
    ax2[2,0].legend(loc = 'best')
    ax2[0,1].scatter(R2F, Vaz2F, s = 1, label = f'Evolution time : t = {tfin}', color = 'red')
    ax2[0,1].set_xlabel('$R_{cyl}$ [nbody units]')
    ax2[0,1].set_ylabel('$V_{\\phi}$ [nbody units]')
    ax2[0,1].set_title("Azimuthal velocity profile")
    ax2[0,1].legend(loc = 'best')
    ax2[1,1].scatter(R2F, Vrad2F, s = 1, label = f'Evolution time : t = {tfin}', color = 'red')
    ax2[1,1].set_xlabel('$R_{cyl}$ [nbody units]')
    ax2[1,1].set_ylabel('$V_{rad}$ [nbody units]')
    ax2[1,1].set_title("Radial velocity profile")
    ax2[1,1].legend(loc = 'best')
    ax2[2,1].plot(Rave2F, Sigma2F, label = f'Evolution time : t = {tfin}', color = 'red')
    ax2[2,1].set_yscale("log")
    ax2[2,1].set_xlabel('$R_{cyl}$ [nbody units]')
    ax2[2,1].set_ylabel('$\Sigma(R_{cyl})$ [nbody units]')
    ax2[2,1].set_title('Surface density profile')
    ax2[2,1].legend(loc = 'best')
    fig2.suptitle(f'{nameplot2} : Azimuthal velocity profile, radial velocity profile and surface density profile for galaxy two')
    plt.tight_layout()
    fig2.savefig(f"{nameplot2}.png")
    plt.show()
    StatisticalAnalysis(Vaz1F, Vaz2F, Vrad1F, Vrad2F, R1F, R2F, ext)

    return fig1, fig2