import matplotlib.pyplot as plt
import numpy as np
from numpy import inf
import time
from fireworks.particles import Particles
from nbodylib.dynamics import acceleration_pyfalcon
import GenParticles as GP
import RotationCurve as RC
import SurfaceDensity as SD
import numpy.typing as npt

class Merge:
    def provide_parameters(self,Npoints: int, final_time: int,
                            pos_shift: npt.NDArray[np.float64],
                            vel_shift: npt.NDArray[np.float64],
                            mass_1: int = 1, mass_2: int = 1,
                            theta: np.float64 = 0.,
                            phi: np.float64 = 0.):
        self.Npoints = Npoints
        self.final_time = final_time
        self.mass_1 = mass_1
        self.mass_2 = mass_2
        self.pos_shift = pos_shift
        self.vel_shift = vel_shift
        self.theta = theta
        self.phi = phi
        self.galaxy_merge = GP.GenerationGalaxyParticles("Nbody_disc.csv", 
                                                    self.Npoints,
                                                    self.pos_shift,
                                                    self.vel_shift,
                                                    self.theta,
                                                    self.phi)
        self.galaxy_one = Particles(self.galaxy_merge.pos[0:1000,:], self.galaxy_merge.vel[0:1000,:], self.galaxy_merge.mass[0:1000])

    def IsolatedGalaxyEvolution(self):
        
        R, Vphi = RC.RotationalCurveFunction(self.galaxy_one)
        Rave, Area, H, R, _ = SD.SDensRotCurve(self.galaxy_one)

        t_end = np.nanmean(2*np.pi*R/Vphi)
        print ("End time : ", t_end)

        fig, axs = plt.subplots(2,2, figsize = (10,9))
        fig.suptitle('Evolution of an isolated  Galaxy')
        axs[0,0].scatter(R, Vphi)
        axs[0,0].set_title('Initial step')
        axs[0,0].set_xlabel('R')
        axs[0,0].set_ylabel('$V_\phi$')

        axs[0,1].plot(Rave, H/Area)
        axs[0,1].set_title('Final step: surface density')
        axs[0,1].set_yscale("log")

        N         = len(self.galaxy_one.mass)    # number of particles
        t         = 0      # current time of the simulation
        endTime   = self.final_time  # time at which simulation ends
        dt        = 0.001   # timestep

        M  =  self.galaxy_one.mass
        M  = M.reshape((len(M),1))
        R  = self.galaxy_one.pos
        vel = self.galaxy_one.vel

        # calculate initial gravitational accelerations

        acc, _, _ = acceleration_pyfalcon(self.galaxy_one, 5.)

        # number of timesteps
        Nt = int(np.ceil(endTime/dt))

        # save particle orbits for plotting trails
        position_save = np.zeros((N, 3, Nt+1))
        position_save[:,:,0] = R

        for i in range(Nt):
                vel += acc * dt/2.0
                R += vel * dt
                acc, _, _ = acceleration_pyfalcon(self.galaxy_one, 5.)
                vel += acc * dt/2.0
                t += dt
                position_save[:,:,i+1] = R
        
        self.galaxy_one.pos = R
        self.galaxy_one.vel = vel

        Rfin, Vphifin = RC.RotationalCurveFunction(self.galaxy_one)
        Ravef, Areaf, Hf, _, _ = SD.SDensRotCurve(self.galaxy_one)

        axs[1,0].scatter(Rfin, Vphifin)
        axs[1,0].set_title('Final step: rotational velocity curve')
        axs[1,0].set_xlabel('R')
        axs[1,0].set_ylabel('$V_\phi$')

        axs[1,1].plot(Ravef, Hf/Areaf)
        axs[1,1].set_title('Final step: surface density')
        axs[1,1].set_yscale("log")

        plt.show()

    def GalaxyMerge(self):
        N         = len(self.galaxy_merge.mass)    # number of particles
        t         = 0      # current time of the simulation
        dt        = 0.01   # timestep
        
        M   =  self.galaxy_merge.mass
        M  = M.reshape((len(M),1))
        R   = self.galaxy_merge.pos
        vel = self.galaxy_merge.vel
        
        # Convert to Center-of-M frame
        vel -= np.mean(M * vel,0) / np.mean(M)
        
        # calculate initial gravitational accelerations

        acc, _, _ = acceleration_pyfalcon(self.galaxy_merge, 5.)
        
        # number of timesteps
        Nt = int(np.ceil(self.final_time/dt))
        
        # save particle orbits for plotting trails
        position_save = np.zeros((N, 3, Nt+1))
        position_save[:,:,0] = R
        
        # figure
        #plt.style.use('dark_background')

        fig = plt.figure()
        ax = fig.add_subplot(projection = '3d')

        # simulation loop
        for i in range(Nt):
            vel += acc * dt/2.0
            R += vel * dt
            acc, _, _ = acceleration_pyfalcon(self.galaxy_merge, 5.)
            vel += acc * dt/2.0
            t += dt
            #position_save[:,:,i+1] = R

            if (i % 50 == 0):
                plt.cla()
                ax.scatter(R[0:int(N/2.),0], R[0:int(N/2.),1], R[0:int(N/2.),2], color = 'blue', marker = '*', s = 5)
                ax.scatter(R[int(N/2.)+1:,0], R[int(N/2.)+1:,1], R[int(N/2.)+1:,2], color = 'red', marker = 'o', s = 5)
                #xp = position_save[:, 0, max(i-50,0):i+1]
                #yp = position_save[:, 1, max(i-50,0):i+1]
                #plt.scatter(xp, yp, s=1, color = 'cornflowerblue')
                #plt.scatter(R[:,0], R[:,1], s=10, color ='white')
                plt.pause(0.001)
        
        plt.show()